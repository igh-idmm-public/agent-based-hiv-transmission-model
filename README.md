**This repository contains the scripts of an agent-based simulation model for HIV transmission.** The present version includes the model itself as well as its adaptation for Malawi in five scenarios (see manuscript https://doi.org/10.1101/2020.12.23.20248757 for details).

The program requires the following R packages: extraDistr, plyr, dplyr, Rcpp

The scripts that contain the functions for running the model:
    **model_main.R** calls the other necessary scripts and runs the entire simulation
    **functions_main.R** contains all functions that are needed to update the modelled population at each time step
    **HIVsimulation_gems.R** simulates the progression of HIV for a cohort of patients, from the time of infection until death. The results of this function will be used to update the infectiousness and time of HIV related death for all HIV infected patients in the simulated population. This function is based on the R package gems.
    **HIVsimulation_light.R** is a simplified version of **HIVsimulation_gems.R** that samples the transition times from constant rates and is therefore multiple times faster

The input parameters for each of the five model (I-V) runs are stored in the sripts parameters_Model_X.R, where X = I to V. 

The file Collecting_outputs contains functions that store the main outcomes of interest (national and location-specific HIV prevalence among adults aged 15-49 years) after each time step. 

To run the model, one must go to the **model_main.R** script, and, if necessary, modify the following information:
    line 6: replace file name "Parameters_Model_I.R" with the desired model (I to V)
    lines 42, 79 and 100: replace file name "HIVsimulation_light.R" with "HIVsimulation_gems.R" if you wish to use the gems version instead of the light version

