immigr <- FALSE
popsize_init <- 5302000 #0
female_prob <- 0.501
age_distr_m <- c(0.156, 0.156, 0.156, 0.073, 0.073, 0.073, 0.073, 0.073, 0.073, 0.073, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003, 0.003)
age_distr_f <- c(0.156, 0.156, 0.156, 0.072, 0.072, 0.072, 0.072, 0.072, 0.072, 0.072, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004, 0.004)
calendar_time <- 1975
calendar_time_end <- 2030

n_locs <- 29
location_probs <-  c(0.012, 0.018, 0.001, 0.057, 0.016, 0.012, 0.059, 0.053, 0.045, 0.127, 0.031, 0.022, 0.045, 0.019, 0.023, 0.029, 0.076, 0.034, 0.022, 0.035, 0.058, 0.040, 0.008, 0.018, 0.043, 0.022, 0.070, 0.005, 0)

n_socbehfact_cats <- 2
prob_socbeh_m <- c(0.95, 0.05)  #cbind(rep(0.95, length=n_locs), rep(0.05, length=n_locs))
prob_socbeh_f <- c(0.95, 0.05) #cbind(rep(0.95, length=n_locs), rep(0.05, length=n_locs))
progr_socbeh_m <- rbind(c(1,0),c(1/25, 24/25)) #array(data=0, dim=c(n_locs, 2, 2))
progr_socbeh_f <- rbind(c(1,0),c(1/10, 9/10)) #array(data=0, dim=c(n_locs, 2, 2))
# for (i in (1:n_locs)) {
#   progr_socbeh_m[i,,] <- rbind(c(1,0), c(1/25, 24/25))
#   progr_socbeh_f[i,,] <- rbind(c(1,0), c(1/10, 9/10))
# }

n_biomedfact_cats <- 2
prob_biomed_m <- c(1, 0) #cbind(rep(1, length=n_locs), rep(0, length=n_locs))
prob_biomed_f <- c(1, 0) #cbind(rep(1, length=n_locs), rep(0, length=n_locs))
progr_biomed_m <- rbind(c(1,0), c(0,1)) #array(data=0, dim=c(n_locs, 2, 2))
progr_biomed_f <- rbind(c(1,0), c(0,1)) #array(data=0, dim=c(n_locs, 2, 2))
# for (i in (1:n_locs)) {
#   progr_biomed_m[i,,] <- rbind(c(1,0), c(0,1))
#   progr_biomed_f[i,,] <- rbind(c(1,0), c(0,1))
# }


seeddistricts <- c(2,11,13,15,(16:28))
inf_seed <- 4000 #0
inf_seed_d <- c(0.25,0.25,0.25,0.25)

###

partnerchange_limit <- 50
partnerchange_young <- 1
partnerchange_old <- 0
wantspartner_m <- 1
wantspartner_f <- 1
regmix_age <- matrix(1, nrow=12, ncol=12)
regmix_behav <- matrix(1, nrow=2, ncol=2)

###

relocation_probability <- matrix(0.01, nrow=n_locs, ncol=calendar_time_end-calendar_time+1)
relocation_probability[c(5,6,9,11,26),] <- 1*0.012
relocation_probability[c(3,12,14,16:19,22:25),] <- 1*0.015
relocation_probability[c(7,8,13,27),] <- 1*0.02
geodistance_reloc <- matrix(Inf, nrow=29, ncol=29)
geodistance_reloc[(1:28),(1:27)] <- 1
geodistance_reloc[(1:28),28] <- 0.5
diag(geodistance_reloc) <- 0

###

sexage_limit <- 50
mean_regacts_young <- 50
sd_regacts_young <- 0
mean_regacts_old <- 0
sd_regacts_old <- 0
cc <- 2
peract_acute_mf <- cc*5*2.95*(1/2.45)*4.3e-5*2.45^log10(10000)
peract_chronic_mf <- cc*2.95*(1/2.45)*4.3e-5*2.45^log10(10000)
peract_treated_mf <- cc*2.95*(1/2.45)*4.3e-5*2.45^log10(10)
peract_failing_mf <- cc*2.95*(1/2.45)*4.3e-5*2.45^log10(1000)
peract_acute_fm <- cc*5*2.95*(1/2.45)*2.2e-5*2.45^log10(10000)
peract_chronic_fm <- cc*2.95*(1/2.45)*2.2e-5*2.45^log10(10000)
peract_treated_fm <- cc*2.95*(1/2.45)*2.2e-5*2.45^log10(10)
peract_failing_fm <- cc*2.95*(1/2.45)*2.2e-5*2.45^log10(1000)
peract_acute_mm <- cc*5*2.95*(1/2.45)*4.3e-4*2.45^log10(10000)
peract_chronic_mm <- cc*2.95*(1/2.45)*4.3e-4*2.45^log10(10000)
peract_treated_mm <- cc*2.95*(1/2.45)*4.3e-4*2.45^log10(10)
peract_failing_mm <- cc*2.95*(1/2.45)*4.3e-4*2.45^log10(1000)

casmix_loc <- matrix(0, nrow=29, ncol=29)
diag(casmix_loc) <- 1
casmix_age <- matrix(1, nrow=12, ncol=12)
casmix_behav <- matrix(1, nrow=2, ncol=2)
mean_caspartners_M <- matrix(0, nrow=2, ncol=12)
mean_caspartners_M[1,] <- 24
mean_caspartners_M[2,] <- 95
mean_caspartners_F <- matrix(0, nrow=2, ncol=12)
mean_caspartners_F[1,] <- 24
mean_caspartners_F[2,] <- 95
mean_casacts <- 1
sd_casacts <- 0
condom <- c(rep(1, length=15), seq(from=1, to=0.25, length=13), rep(0.25, length=50))

###
cccc <- 0.9
birthrate_15_20 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_20_25 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_25_30 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_30_35 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_35_40 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_40_45 <- cccc*rep(1.34*5.47/35, length=75)
birthrate_45_50 <- cccc*rep(1.34*5.47/35, length=75)

###

mtct_p_untreated <- 0.35
mtct_p_treated <- 0.05
mtct_p_prophyl <- 0.20
pmtctcoverage <- 1

###

malemortality <- c(rep(0.0142, length=15), rep(0.00594, length=35), rep(1/20.9, length=40), rep(100000, length=46))
femalemortality <- c(rep(0.0142, length=15), rep(0.00506, length=35), rep(1/23.7, length=46), rep(100000, length=40))

###

diagrate_infant <- 1-exp(-10)
diagrate_pregwoman <- 1-exp(-1)
diagrate_general <- 1-exp(-0.05)

treatmentrate_early <- 1-exp(-0.07)
treatmentrate_current <- 1-exp(-0.4)
treatmentrate_future <- 1-exp(-1)

vlfrate <- 0.05 #1-exp(0.05)
falseswitchrate <- 0.01 #1-exp(-0.01)
nonVLswitchrate <- 0.2 #1-exp(-0.2)
VLswitchrate <- 1 #1-exp(-1)
secondvlfrate <- 0.05 #1-exp(-0.05)

dropout_early <- 0.25 #1-exp(-0.25)
dropout_late <- 0.1 #1-exp(-0.1)
dropout_second <- 0.1
#b2c <- 1-exp(-0.33)
b2crate <- 0.33

HIVmort_acute <- 0
HIVmort_chronic <- 1-exp(-0.05)
HIVmort_treated <- 1-exp(-0.01)
HIVmort_treated_failing <- 1-exp(-0.1)








